package au.id.itch.splendor;

import au.id.itch.splendor.domain.Game;
import au.id.itch.splendor.domain.GameFactory;
import au.id.itch.splendor.repository.CardRepository;
import au.id.itch.splendor.repository.GameRepository;
import au.id.itch.splendor.repository.NobleRepository;
import au.id.itch.splendor.web.TakeTokensVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SplendorApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class SplendorApplicationTest {

    @Value("${local.server.port}")
    private int port;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private NobleRepository nobleRepository;

    @Test
    public void testGame() throws Exception {
        Game game = GameFactory.create("Mygame", Arrays.asList("Richard", "Lizzy", "Wen", "Chris"), cardRepository.findAll(), nobleRepository.findAll());
        gameRepository.save(game);

        ResponseEntity<String> entity = new TestRestTemplate().getForEntity(
                "http://localhost:" + this.port + "/status?username=Richard", String.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertThat(entity.getBody(), containsString("\"currentPlayer\":\"Richard\""));
        System.out.println(entity.getBody());
    }

    @Test
    public void testTakeTokensWithoutUsernameReturnsBadRequest() throws Exception {
        Game game = GameFactory.create("Mygame", Arrays.asList("Richard", "Lizzy", "Wen", "Chris"), cardRepository.findAll(), nobleRepository.findAll());
        gameRepository.save(game);

        TakeTokensVO takeTokensVO = new TakeTokensVO();

        ResponseEntity<Object> entity = new TestRestTemplate().postForEntity(
                "http://localhost:" + this.port + "/takeTokens", takeTokensVO, Object.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        System.out.println(entity.getBody());
    }
}