package au.id.itch.splendor.service;

import au.id.itch.splendor.domain.Game;
import au.id.itch.splendor.repository.*;
import org.junit.Test;

import java.util.Collection;

public class GameServiceTest {

    private GameRepository gameRepository = new InMemoryGameRepository();
    private CardRepository cardRepository = new FileCardRepository();
    private NobleRepository nobleRepository = new FileNobleRepository();
    private GameService gameService = new GameService(gameRepository, cardRepository, nobleRepository);

    @Test
    public void testGameService() throws Exception {
        Collection<Game> allGames = gameService.getAllGames();
        System.out.println("allGames = " + allGames);

    }

}