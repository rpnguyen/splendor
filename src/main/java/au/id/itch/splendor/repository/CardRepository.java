package au.id.itch.splendor.repository;

import au.id.itch.splendor.domain.Card;

import java.util.Collection;

public interface CardRepository {
    Card findOne(Integer id);

    Collection<Card> findAll();
}
