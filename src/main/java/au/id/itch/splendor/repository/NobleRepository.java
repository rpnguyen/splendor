package au.id.itch.splendor.repository;

import au.id.itch.splendor.domain.Noble;

import java.util.Collection;

public interface NobleRepository {
    Noble findOne(Integer id);

    Collection<Noble> findAll();
}
