package au.id.itch.splendor.repository;

import au.id.itch.splendor.domain.Game;

import java.util.Collection;

public interface GameRepository {
    Game save(Game game);

    Game findOne(String name);

    Collection<Game> findAll();

    void delete(String name);

    long count();

    Game findOneByUser(String username);
}
