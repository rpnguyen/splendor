package au.id.itch.splendor.repository;

import au.id.itch.splendor.domain.Card;
import au.id.itch.splendor.domain.ColourCount;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class FileCardRepository implements CardRepository {

    @Value("classpath:cards.json")
    private Resource cards;

    private Map<Integer, Card> cardMap = new ConcurrentHashMap<>();

    @Override
    public Card findOne(Integer id) {
        return cardMap.get(id);
    }

    @Override
    public Collection<Card> findAll() {
        return cardMap.values();
    }

    @PostConstruct
    private void init() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Card.class, new CardDeserializer());
        mapper.registerModule(module);

        cardMap = mapper.readValue(cards.getInputStream(), new TypeReference<Map<Integer, Card>>() {
        });
    }

    private class CardDeserializer extends JsonDeserializer<Card> {
        @Override
        public Card deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);

            Card.Level level = Card.Level.get(node.get("level").asText());
            int points = node.get("points").asInt(-1);
            Card.Colour colour = Card.Colour.get(node.get("colour").asText());
            JsonNode priceNode = node.get("cost");
            final int whitePrice = priceNode.get("white").asInt(-1);
            final int bluePrice = priceNode.get("blue").asInt(-1);
            final int greenPrice = priceNode.get("green").asInt(-1);
            final int redPrice = priceNode.get("red").asInt(-1);
            final int blackPrice = priceNode.get("black").asInt(-1);

            ColourCount price = new ColourCount(whitePrice, bluePrice, greenPrice, redPrice, blackPrice);

            int id = Integer.parseInt(jsonParser.getCurrentName());

            return new Card(id, level, points, colour, price);
        }
    }

}
