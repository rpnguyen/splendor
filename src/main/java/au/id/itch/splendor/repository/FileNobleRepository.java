package au.id.itch.splendor.repository;

import au.id.itch.splendor.domain.ColourCount;
import au.id.itch.splendor.domain.Noble;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class FileNobleRepository implements NobleRepository {

    @Value("classpath:nobles.json")
    private Resource nobles;

    private Map<Integer, Noble> nobleMap = new ConcurrentHashMap<>();

    @Override
    public Noble findOne(Integer id) {
        return nobleMap.get(id);
    }

    @Override
    public Collection<Noble> findAll() {
        return nobleMap.values();
    }

    @PostConstruct
    private void init() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Noble.class, new NobleDeserializer());
        mapper.registerModule(module);

        nobleMap = mapper.readValue(nobles.getInputStream(), new TypeReference<Map<Integer, Noble>>() {
        });
    }

    private class NobleDeserializer extends JsonDeserializer<Noble> {
        @Override
        public Noble deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);

            int points = node.get("points").asInt(-1);
            JsonNode priceNode = node.get("cost");
            final int whitePrice = priceNode.get("white").asInt(-1);
            final int bluePrice = priceNode.get("blue").asInt(-1);
            final int greenPrice = priceNode.get("green").asInt(-1);
            final int redPrice = priceNode.get("red").asInt(-1);
            final int blackPrice = priceNode.get("black").asInt(-1);

            ColourCount price = new ColourCount(whitePrice, bluePrice, greenPrice, redPrice, blackPrice);

            int id = Integer.parseInt(jsonParser.getCurrentName());

            return new Noble(id, points, price);
        }
    }

}
