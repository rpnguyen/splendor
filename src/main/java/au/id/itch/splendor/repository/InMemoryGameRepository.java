package au.id.itch.splendor.repository;

import au.id.itch.splendor.domain.Game;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InMemoryGameRepository implements GameRepository {

    private Map<String, Game> gameMap = new ConcurrentHashMap<>();

    @Override
    public Game save(Game game) {
        gameMap.put(game.getName(), game);
        return game;
    }

    @Override
    public Game findOne(String name) {
        return gameMap.get(name);
    }

    @Override
    public Collection<Game> findAll() {
        return gameMap.values();
    }

    @Override
    public void delete(String name) {
        gameMap.remove(name);
    }

    @Override
    public long count() {
        return gameMap.size();
    }

    @Override
    public Game findOneByUser(String username) {
        return gameMap.values().stream()
                .filter(g -> g.containsPlayer(username))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(username + " is not in a game"));
    }
}
