package au.id.itch.splendor.service;

import au.id.itch.splendor.domain.Game;
import au.id.itch.splendor.domain.GameFactory;
import au.id.itch.splendor.domain.Player;
import au.id.itch.splendor.domain.TokenCount;
import au.id.itch.splendor.repository.CardRepository;
import au.id.itch.splendor.repository.GameRepository;
import au.id.itch.splendor.repository.NobleRepository;
import au.id.itch.splendor.web.BuyCardVO;
import au.id.itch.splendor.web.ReserveCardVO;
import au.id.itch.splendor.web.TakeTokensVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class GameService {

    private GameRepository gameRepository;

    private CardRepository cardRepository;

    private NobleRepository nobleRepository;

    @Autowired
    public GameService(GameRepository gameRepository,
                       CardRepository cardRepository,
                       NobleRepository nobleRepository) {
        this.gameRepository = gameRepository;
        this.cardRepository = cardRepository;
        this.nobleRepository = nobleRepository;
    }

    public Game createGame(String name, List<String> playerNames) {
        // TODO check if < 2 unique names, then throw exception
        // TODO Check whether any of these players exist in a game yet
        Game game = GameFactory.create(name, playerNames, cardRepository.findAll(), nobleRepository.findAll());
        return gameRepository.save(game);
    }

    public void deleteGame(String name) {
        gameRepository.delete(name);
    }

    public Game getGame(String name) {
        return gameRepository.findOne(name);
    }

    public Collection<Game> getAllGames() {
        return gameRepository.findAll();
    }

    public Game takeTokens(TakeTokensVO takeTokensVO) {
        Game game = getGameByUser(takeTokensVO.username);
        Player player = game.getPlayerByUsername(takeTokensVO.username);
        if (!player.equals(game.getCurrentPlayer())) {
            throw new IllegalStateException("It is not " + player.getName() + "'s turn");
        }

        game.giveTokensToPlayer(player, new TokenCount(takeTokensVO.tokens));
        game.receiveTokensFromPlayer(player, new TokenCount(takeTokensVO.discard));
        if (takeTokensVO.noble != null) {
            game.giveNobleToPlayer(player, nobleRepository.findOne(takeTokensVO.noble));
        }
        game.advanceTurn();
        return game;
    }

    public Game buyCard(BuyCardVO buyCardVO) throws Exception {
        Game game = getGameByUser(buyCardVO.username);
        Player player = game.getPlayerByUsername(buyCardVO.username);
        if (!player.equals(game.getCurrentPlayer())) {
            throw new IllegalStateException("It is not " + player.getName() + "'s turn");
        }

        game.purchaseCard(player, cardRepository.findOne(buyCardVO.card), new TokenCount(buyCardVO.pay));

        if (buyCardVO.noble != null) {
            game.giveNobleToPlayer(player, nobleRepository.findOne(buyCardVO.noble));
        }
        game.advanceTurn();
        return game;
    }

    public Game reserveCard(ReserveCardVO reserveCardVO) throws Exception {
        Game game = getGameByUser(reserveCardVO.username);
        Player player = game.getPlayerByUsername(reserveCardVO.username);
        if (!player.equals(game.getCurrentPlayer())) {
            throw new IllegalStateException("It is not " + player.getName() + "'s turn");
        }

        if (reserveCardVO.card != null) {
            game.reserveCard(player, cardRepository.findOne(reserveCardVO.card), reserveCardVO.tokens.getOrDefault("gold", 0));
        } else if (reserveCardVO.deck != null) {
            game.reserveTopOfDeck(player, reserveCardVO.deck, reserveCardVO.tokens.getOrDefault("gold", 0));
        }
        game.receiveTokensFromPlayer(player, new TokenCount(reserveCardVO.discard));
        if (reserveCardVO.noble != null) {
            game.giveNobleToPlayer(player, nobleRepository.findOne(reserveCardVO.noble));
        }
        game.advanceTurn();
        return game;
    }

    public Game getGameByUser(String username) {
        return gameRepository.findOneByUser(username);
    }
}