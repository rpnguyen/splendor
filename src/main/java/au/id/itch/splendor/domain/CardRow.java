package au.id.itch.splendor.domain;

import java.util.List;

/**
 * Created by Wen on 4/12/2014.
 */
public class CardRow {

    List<Card> cards;

    public CardRow(List<Card> cards) {
        this.cards = cards;
    }

    /**
     * Replace a card on the row with another card
     * @param oldCard
     * @param newCard
     * @return
     */
    public void replace(Card oldCard, Card newCard) {
        final int index = cards.indexOf(oldCard);
        cards.set(index, newCard);
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public List<Card> getCards() {
        return cards;
    }
}
