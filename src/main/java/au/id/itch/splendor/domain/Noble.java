package au.id.itch.splendor.domain;

public class Noble {

    private final int id;
    private final int points;
    private final ColourCount price;

    public Noble(int id, int points, ColourCount price) {
        this.id = id;
        this.points = points;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public int getPoints() {
        return points;
    }

    public ColourCount getPrice() { return price; }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Noble{");
        result.append("id:");
        result.append(id);
        result.append(",points:");
        result.append(points);
        result.append(",price:");
        result.append(price);
        result.append("}");
        return result.toString();
    }
}
