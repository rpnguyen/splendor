package au.id.itch.splendor.domain;

/**
 * Created by Wen on 27/11/2014.
 */
public class Card {

    public enum Level {
        ONE(1),
        TWO(2),
        THREE(3);

        private final int id;

        private Level(int id) {
            this.id = id;
        }

        public static Level get(String level) {
            switch (level) {
                case "1":
                    return ONE;
                case "2":
                    return TWO;
                case "3":
                    return THREE;
                default:
                    throw new IllegalArgumentException("No such level as " + level);
            }
        }

        public int getId() {
            return id;
        }
    }

    public enum Colour {
        WHITE,
        BLUE,
        GREEN,
        RED,
        BLACK;

        public static Colour get(String colour) {
            switch (colour) {
                case "white":
                    return WHITE;
                case "blue":
                    return BLUE;
                case "green":
                    return GREEN;
                case "red":
                    return RED;
                case "black":
                    return BLACK;
                default:
                    throw new IllegalArgumentException("No such colour as " + colour);
            }
        }
    }

    private final int id;
    private final Level level;
    private final Colour colour;
    private final int points;
    private final ColourCount price;

    public Card(int id, Level level, int points, Colour colour, ColourCount price) {
        this.id = id;
        this.level = level;
        this.points = points;
        this.colour = colour;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public Level getLevel() {
        return level;
    }

    public Colour getColour() {
        return colour;
    }

    public int getPoints() {
        return points;
    }

    public ColourCount getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof Card)) {
            return false;
        }
        if (((Card) other).id == this.id) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Card{");
        result.append("id:");
        result.append(id);
        result.append(",level:");
        result.append(level);
        result.append(",color:");
        result.append(colour);
        result.append(",points:");
        result.append(points);
        result.append(",price:");
        result.append(price);
        result.append("}");
        return result.toString();
    }
}
