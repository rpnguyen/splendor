package au.id.itch.splendor.domain;

import java.util.Map;

/**
 * Created by Wen on 3/12/2014.
 */
public class TokenCount extends ColourCount {

    private int gold = 0;

    public TokenCount() {
        this.gold = 0;
    }

    public TokenCount(int white, int blue, int green, int red, int black, int gold) {
        super(white, blue, green, red, black);
        this.gold = gold;
    }

    public TokenCount(Map<String, Integer> map) {
        super(
                map.getOrDefault("white", 0),
                map.getOrDefault("blue", 0),
                map.getOrDefault("green", 0),
                map.getOrDefault("red", 0),
                map.getOrDefault("black", 0));
        this.gold = map.getOrDefault("gold", 0);
    }

    public int getGold() {
        return gold;
    }

    public TokenCount add(TokenCount otherCount) {
        this.gold += otherCount.gold;
        super.add(otherCount);
        return this;
    }

    public TokenCount subtract(TokenCount otherCount) {
        this.gold -= otherCount.gold;
        super.subtract(otherCount);
        return this;
    }

    public boolean isGreaterThan(TokenCount otherCount) {
        return this.gold >= otherCount.gold && super.isGreaterThanOrEqualTo(otherCount);
    }

    public void addGold(int token) {
        this.gold += token;
    }
}
