package au.id.itch.splendor.domain;


import java.util.Collections;
import java.util.Random;
import java.util.Stack;

/**
 * Created by Wen on 27/11/2014.
 */
public class Deck {

    private final Stack<Card> cards;

    public Deck() {
        this.cards = new Stack<Card>();
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public Card pop() {
        return cards.pop();
    }

    public Card peek() {
        return cards.peek();
    }

    public void shuffle() {
        Collections.shuffle(cards, new Random(System.nanoTime()));
    }


    public int size() {
        return cards.size();
    }

}
