package au.id.itch.splendor.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wen on 27/11/2014.
 */
public class Player {

    private final String name;
    private final List<Card> cardsOwned = new ArrayList<Card>();
    private final List<Card> cardsReserved = new ArrayList<Card>();
    private final List<Noble> nobles = new ArrayList<>();

    private TokenCount tokens = new TokenCount();

    public Player(String name) {
        this.name = name;
    }

    public void purchaseCard(Card card, TokenCount tokensPaid) throws Exception {
        validatePurchase(card, tokensPaid);
        tokens.subtract(tokensPaid);
        cardsOwned.add(card);
        if (hasCardInReserve(card)) {
            cardsReserved.remove(card);
        }
    }

    public void reserveCard(Card card, int goldTaken) {
        cardsReserved.add(card);
        tokens.addGold(goldTaken);
    }

    public void addTokens(TokenCount otherTokens) {
        tokens.add(otherTokens);
    }

    public void subtractTokens(TokenCount otherTokens) {
        tokens.subtract(otherTokens);
    }

    public ColourCount getDiscount() throws Exception {
        ColourCount discount = new ColourCount();
        for (Card card : cardsOwned) {
            switch (card.getColour()) {
                case WHITE:
                    discount.addWhite(1);
                    break;
                case BLUE:
                    discount.addBlue(1);
                    break;
                case GREEN:
                    discount.addGreen(1);
                    break;
                case RED:
                    discount.addRed(1);
                    break;
                case BLACK:
                    discount.addBlack(1);
                    break;
                default:
                    throw new Exception("Not a valid colour. This should now happen!");
            }
        }
        return discount;
    }

    public void earnNoble(Noble noble) {
        nobles.add(noble);
    }

    public boolean hasCardInReserve(Card card) {
        return cardsReserved.contains(card);
    }

    public void validatePurchase(Card card, TokenCount tokensPaid) throws Exception {
        if (!tokens.isGreaterThanOrEqualTo(tokensPaid)) {
            throw new Exception("Insufficient funds.");
        }

        final ColourCount discount = getDiscount();
        int whitePrice = Math.max(card.getPrice().getWhite() - discount.getWhite(), 0);
        int bluePrice = Math.max(card.getPrice().getBlue() - discount.getBlue(), 0);
        int greenPrice = Math.max(card.getPrice().getGreen() - discount.getGreen(), 0);
        int redPrice = Math.max(card.getPrice().getRed() - discount.getRed(), 0);
        int blackPrice = Math.max(card.getPrice().getBlack() - discount.getBlack(), 0);

        int goldRequired = 0;
        goldRequired += whitePrice - tokensPaid.getWhite();
        goldRequired += bluePrice - tokensPaid.getBlue();
        goldRequired += greenPrice - tokensPaid.getGreen();
        goldRequired += redPrice - tokensPaid.getRed();
        goldRequired += blackPrice - tokensPaid.getBlack();

        if (goldRequired != tokensPaid.getGold()) {
            throw new Exception("Incorrect amount paid.");
        }
    }

    public String getName() {
        return name;
    }

    public TokenCount getTokens() {
        return tokens;
    }

    public List<Card> getCardsOwned() {
        return cardsOwned;
    }

    public List<Card> getCardsReserved() {
        return cardsReserved;
    }

    public List<Noble> getNobles() {
        return nobles;
    }

    public int getPointsEarned() {
        int points = 0;
        for (Card card : cardsOwned) {
            points += card.getPoints();
        }
        for (Noble noble : nobles) {
            points += noble.getPoints();
        }
        return points;
    }
}
