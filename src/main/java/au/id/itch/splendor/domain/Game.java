package au.id.itch.splendor.domain;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Wen on 27/11/2014.
 */
public class Game {

    private boolean complete;
    private final String name;
    private final List<Player> players;
    private Player currentPlayer;

    private final Map<Card.Level, Deck> decks;
    private final Map<Card.Level, CardRow> cardRows;
    private final List<Noble> nobles;
    private final TokenCount tokens;

    public Game(String name,
                List<Player> players,
                Player currentPlayer,
                Map<Card.Level, Deck> decks,
                Map<Card.Level, CardRow> cardRows,
                List<Noble> nobles,
                TokenCount tokens) {
        this.name = name;
        this.players = players;
        this.currentPlayer = currentPlayer;
        this.decks = decks;
        this.cardRows = cardRows;
        this.nobles = nobles;
        this.tokens = tokens;
    }

    public void giveTokensToPlayer(Player player, TokenCount input) {
        tokens.subtract(input);
        player.addTokens(input);
    }

    public void receiveTokensFromPlayer(Player player, TokenCount input) {
        tokens.add(input);
        player.subtractTokens(input);
    }

    public boolean reserveCard(Player player, Card card, int goldTokens) throws Exception {
        CardRow cardRow = cardRows.get(card.getLevel());
        if (cardRow == null) {
            throw new Exception("Could not find card level in revealed cards.");
        }
        if (cardRow.contains(card)) {
            Deck deck = decks.get(card.getLevel());
            cardRow.replace(card, deck.pop());
            player.reserveCard(card, goldTokens);

            // Subtract appropriate amount of gold tokens from common area
            Map<String, Integer> tokenCountMap = new HashMap<>();
            tokenCountMap.put("gold", goldTokens);
            tokens.subtract(new TokenCount(tokenCountMap));
            return true;
        }
        return false;
    }

    public boolean reserveTopOfDeck(Player player, String level, int goldTokens) throws Exception {
        Deck deck = decks.get(Card.Level.get(level));
        if (deck == null) {
            throw new Exception("Could not find card level in decks.");
        }
        player.reserveCard(deck.pop(), goldTokens);
        return true;
    }

    public boolean purchaseCard(Player player, Card card, TokenCount tokensPaid) throws Exception {
        player.purchaseCard(card, tokensPaid);
        tokens.add(tokensPaid);

        final CardRow cardRow = cardRows.get(card.getLevel());
        if (cardRow == null) {
            throw new Exception("Could not find card level in revealed cards.");
        }
        if (cardRow.contains(card)) {
            Deck deck = decks.get(card.getLevel());
            if (deck == null) {
                throw new Exception("Could not find card level in decks.");
            }
            cardRow.replace(card, deck.pop());
        }
        return true;
    }

    public void giveNobleToPlayer(Player player, Noble noble) {
        Iterator<Noble> it = nobles.iterator();
        while (it.hasNext()) {
            Noble next = it.next();
            if (next.equals(noble)) {
                it.remove();
            }
        }
        player.earnNoble(noble);
    }

    public Player getPlayerByUsername(String username) {
        for (Player player : players) {
            if (username.equals(player.getName())) {
                return player;
            }
        }
        return null;
    }

    public boolean containsPlayer(String username) {
        return getPlayerByUsername(username) != null;
    }

    public String getName() {
        return name;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Map<Card.Level, Deck> getDecks() {
        return decks;
    }

    public Map<Card.Level, CardRow> getCardRows() {
        return cardRows;
    }

    public List<Noble> getNobles() {
        return nobles;
    }

    public TokenCount getTokens() {
        return tokens;
    }

    public boolean isComplete() {
        return complete;
    }

    @Override
    public String toString() {
        return "Game{" +
                "name='" + name + '\'' +
                ", players=" + players +
                ", currentPlayer=" + currentPlayer +
                ", decks=" + decks +
                ", cardRows=" + cardRows +
                ", nobles=" + nobles +
                ", tokens=" + tokens +
                '}';
    }

    public void advanceTurn() {
        Iterator<Player> it = players.iterator();
        while (it.hasNext()) {
            Player next = it.next();
            if (next.equals(currentPlayer)) {
                if (it.hasNext()) {
                    currentPlayer = it.next();
                } else {
                    startNewRound();
                }
            }
        }
    }

    private void startNewRound() {
        if (players.stream().anyMatch(p -> p.getPointsEarned() >= 15)) {
            currentPlayer = null;
            complete = true;
        } else {
            currentPlayer = players.get(0);
        }
    }
}
