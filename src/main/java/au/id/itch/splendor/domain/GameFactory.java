package au.id.itch.splendor.domain;

import au.id.itch.splendor.domain.Card.Level;

import java.util.*;

public class GameFactory {

    public static Game create(String name, List<String> playerNames, Collection<Card> cards, Collection<Noble> nobles) {
        Map<Level, Deck> decks = buildDecks(cards);
        Map<Level, CardRow> cardRows = buildCardRows(decks);

        Collection<Noble> noblesChosen = pickNobles(nobles, playerNames.size());

        final int numNormalColours;
        if (playerNames.size() == 2) {
            numNormalColours = 4;
        } else if (playerNames.size() == 3) {
            numNormalColours = 5;
        } else {
            numNormalColours = 7;
        }

        TokenCount tokenCount = new TokenCount(numNormalColours, numNormalColours, numNormalColours, numNormalColours, numNormalColours, 5);

        List<Player> players = new ArrayList<>();
        for (String player : playerNames) {
            players.add(new Player(player));
        }
        return new Game(name, players, players.get(0), decks, cardRows, new ArrayList<>(noblesChosen), tokenCount);
    }

    private static Map<Level, CardRow> buildCardRows(Map<Level, Deck> decks) {
        Map<Level, CardRow> cardRows = new HashMap<>();
        for (Map.Entry<Level, Deck> entry : decks.entrySet()) {
            List<Card> cards = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                cards.add(decks.get(entry.getKey()).pop());
            }
            cardRows.put(entry.getKey(), new CardRow(cards));
        }
        return cardRows;
    }

    private static Map<Level, Deck> buildDecks(Iterable<Card> cards) {
        Map<Level, Deck> decks = new HashMap<>();

        decks.put(Level.ONE, new Deck());
        decks.put(Level.TWO, new Deck());
        decks.put(Level.THREE, new Deck());

        for (Card card : cards) {
            decks.get(card.getLevel()).addCard(card);
        }
        shuffleDecks(decks);
        return decks;
    }

    private static void shuffleDecks(Map<Level, Deck> decks) {
        for (Deck deck : decks.values()) {
            deck.shuffle();
        }
    }

    private static Collection<Noble> pickNobles(Collection<Noble> nobles, int playerCount) {
        List<Noble> noblesList = new ArrayList<>(nobles);

        Collections.shuffle(noblesList);

        return noblesList.subList(0, playerCount + 1); // we need n+1 nobles.

    }

}
