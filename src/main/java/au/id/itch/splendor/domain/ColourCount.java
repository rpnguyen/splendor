package au.id.itch.splendor.domain;

/**
 * Created by Wen on 3/12/2014.
 */
public class ColourCount {

    private int white = 0;
    private int blue = 0;
    private int green = 0;
    private int red = 0;
    private int black = 0;

    public ColourCount() {
        white = 0;
        blue = 0;
        green = 0;
        red = 0;
        black = 0;
    }

    public ColourCount(int white, int blue, int green, int red, int black) {
        this.white = white;
        this.blue = blue;
        this.green = green;
        this.red = red;
        this.black = black;
    }

    public int getWhite() {
        return white;
    }

    public int getBlue() {
        return blue;
    }

    public int getGreen() {
        return green;
    }

    public int getRed() {
        return red;
    }

    public int getBlack() {
        return black;
    }

    public ColourCount add(ColourCount otherCount) {
        this.white += otherCount.white;
        this.blue += otherCount.blue;
        this.green += otherCount.green;
        this.red += otherCount.red;
        this.black += otherCount.black;

        return this;
    }

    public ColourCount subtract(ColourCount otherCount) {
        this.white -= otherCount.white;
        this.blue -= otherCount.blue;
        this.green -= otherCount.green;
        this.red -= otherCount.red;
        this.black -= otherCount.black;

        return this;
    }

    public void addWhite(int white) {
        this.white += white;
    }

    public void addBlue(int blue) {
        this.blue += blue;
    }

    public void addGreen(int green) {
        this.green += green;
    }

    public void addRed(int red) {
        this.red += red;
    }

    public void addBlack(int black) {
        this.black += black;
    }

    public boolean isGreaterThanOrEqualTo(ColourCount otherCount) {
        return this.white >= otherCount.white
                && this.blue >= otherCount.blue
                && this.green >= otherCount.green
                && this.red >= otherCount.red
                && this.black >= otherCount.black;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("ColourCount{");
        result.append("white:");
        result.append(white);
        result.append(",blue:");
        result.append(blue);
        result.append(",green:");
        result.append(green);
        result.append(",red:");
        result.append(red);
        result.append(",black:");
        result.append(black);
        result.append("}");
        return result.toString();
    }
}
