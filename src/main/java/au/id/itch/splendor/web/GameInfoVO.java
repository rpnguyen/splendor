package au.id.itch.splendor.web;

import au.id.itch.splendor.domain.Game;
import au.id.itch.splendor.domain.Player;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class GameInfoVO {

    public String name;
    public List<String> playerNames;

    public static GameInfoVO fromGame(Game game) {
        GameInfoVO gameInfoVO = new GameInfoVO();
        gameInfoVO.name = game.getName();
        gameInfoVO.playerNames = game.getPlayers().stream()
                        .map(Player::getName)
                        .collect(toList());
        return gameInfoVO;
    }
}
