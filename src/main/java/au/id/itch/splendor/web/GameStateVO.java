package au.id.itch.splendor.web;

import au.id.itch.splendor.domain.*;
import au.id.itch.splendor.domain.Card.Level;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class GameStateVO {

    public boolean complete;
    public String currentPlayer;
    public List<String> playerList;
    public SharedVO shared;
    public Map<String, PlayerVO> players;

    public static GameStateVO fromGame(Game game) {
        GameStateVO gameStateVO = new GameStateVO();

        gameStateVO.complete = game.isComplete();

        gameStateVO.currentPlayer = (game.getCurrentPlayer() == null) ? null : game.getCurrentPlayer().getName();

        gameStateVO.playerList = game.getPlayers().stream()
                .map(Player::getName)
                .collect(toList());

        gameStateVO.shared = SharedVO.fromGame(game);

        gameStateVO.players = game.getPlayers().stream()
                .collect(toMap(Player::getName, PlayerVO::fromPlayer));

        return gameStateVO;
    }

    private static class SharedVO {
        public List<Integer> cards;
        public Map<String, Integer> remaining;
        public List<Integer> nobles;
        public Map<String, Integer> tokens;

        public static SharedVO fromGame(Game game) {
            SharedVO sharedVO = new SharedVO();

            sharedVO.cards = game.getCardRows().values().stream()
                    .flatMap(cardRow -> cardRow.getCards().stream())
                    .map(Card::getId)
                    .collect(toList());

            sharedVO.remaining = game.getDecks().entrySet().stream()
                    .collect(toMap(
                            (Entry<Level, Deck> e) -> Integer.toString(e.getKey().getId()),
                            (Entry<Level, Deck> e) -> e.getValue().size()));

            sharedVO.nobles = game.getNobles().stream()
                    .map(Noble::getId)
                    .collect(toList());

            sharedVO.tokens = new HashMap<>();
            sharedVO.tokens.put("white", game.getTokens().getWhite());
            sharedVO.tokens.put("blue", game.getTokens().getBlue());
            sharedVO.tokens.put("green", game.getTokens().getGreen());
            sharedVO.tokens.put("red", game.getTokens().getRed());
            sharedVO.tokens.put("black", game.getTokens().getBlack());
            sharedVO.tokens.put("gold", game.getTokens().getGold());

            return sharedVO;
        }
    }

    private static class PlayerVO {
        public List<Integer> cards;
        public List<Integer> reservedCards;
        public List<Integer> nobles;
        public Map<String, Integer> tokens;

        public static PlayerVO fromPlayer(Player player) {
            PlayerVO playerVO = new PlayerVO();
            playerVO.cards = player.getCardsOwned().stream()
                    .map(Card::getId)
                    .collect(toList());

            playerVO.reservedCards = player.getCardsReserved().stream()
                    .map(Card::getId)
                    .collect(toList());

            playerVO.nobles = player.getNobles().stream()
                    .map(Noble::getId)
                    .collect(toList());

            playerVO.tokens = new HashMap<>();
            playerVO.tokens.put("white", player.getTokens().getWhite());
            playerVO.tokens.put("blue", player.getTokens().getBlue());
            playerVO.tokens.put("green", player.getTokens().getGreen());
            playerVO.tokens.put("red", player.getTokens().getRed());
            playerVO.tokens.put("black", player.getTokens().getBlack());
            playerVO.tokens.put("gold", player.getTokens().getGold());

            return playerVO;
        }
    }
}