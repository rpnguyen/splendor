package au.id.itch.splendor.web;

import java.util.Map;

public class TakeTokensVO {
    public String username;
    public Map<String, Integer> tokens;
    public Map<String, Integer> discard;
    public Integer noble;
}
