package au.id.itch.splendor.web;

import java.util.Map;

public class BuyCardVO {
    public String username;
    public Integer card;
    public Integer noble;
    public Map<String, Integer> pay;
}
