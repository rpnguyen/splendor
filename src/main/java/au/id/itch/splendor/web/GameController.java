package au.id.itch.splendor.web;

import au.id.itch.splendor.domain.Game;
import au.id.itch.splendor.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class GameController {

    @Autowired
    private GameService gameService;

    @RequestMapping(value = "/game", method = GET)
    public List<GameInfoVO> listGames() {
        Collection<Game> allGames = gameService.getAllGames();
        return allGames.stream().map(GameInfoVO::fromGame).collect(toList());
    }

    @RequestMapping(value = "/game", method = POST)
    public GameInfoVO createNewGame(@RequestBody GameInfoVO gameInfoVO) {
        Game game = gameService.createGame(gameInfoVO.name, gameInfoVO.playerNames);
        return GameInfoVO.fromGame(game);
    }

    @RequestMapping(value = "/game/{name}", method = DELETE)
    public void deleteGame(@PathVariable(value = "name") String name) {
        gameService.deleteGame(name);
    }

    @RequestMapping("/status")
    public GameStateVO getStatus(@RequestParam("username") String username) {
        Game game = gameService.getGameByUser(username);
        return GameStateVO.fromGame(game);
    }

    @RequestMapping(value = "/takeTokens", method = POST)
    public GameStateVO takeTokens(@RequestBody TakeTokensVO takeTokensVO) {
        if (takeTokensVO.username == null || takeTokensVO.username.isEmpty()) {
            throw new IllegalArgumentException("The 'username' parameter must not be null or empty");
        }
        Game game = gameService.takeTokens(takeTokensVO);
        return GameStateVO.fromGame(game);
    }

    @RequestMapping(value = "/buyCard", method = POST)
    public GameStateVO buyCard(@RequestBody BuyCardVO buyCardVO) throws Exception {
        if (buyCardVO.username == null || buyCardVO.username.isEmpty()) {
            throw new IllegalArgumentException("The 'username' parameter must not be null or empty");
        }
        Game game = gameService.buyCard(buyCardVO);
        return GameStateVO.fromGame(game);
    }

    @RequestMapping(value = "/reserveCard", method = POST)
    public GameStateVO reserveCard(@RequestBody ReserveCardVO reserveCardVO) throws Exception {
        if (reserveCardVO.username == null || reserveCardVO.username.isEmpty()) {
            throw new IllegalArgumentException("The 'username' parameter must not be null or empty");
        }
        Game game = gameService.reserveCard(reserveCardVO);
        return GameStateVO.fromGame(game);
    }

    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(BAD_REQUEST.value());
    }

    @ExceptionHandler
    void handleIllegalStateException(IllegalStateException e, HttpServletResponse response) throws IOException {
        response.sendError(BAD_REQUEST.value());
    }

}