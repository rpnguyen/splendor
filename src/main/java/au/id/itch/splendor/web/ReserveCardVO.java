package au.id.itch.splendor.web;

import java.util.Map;

public class ReserveCardVO {
    public String username;
    public String deck;
    public Integer card;
    public Map<String, Integer> tokens;
    public Map<String, Integer> discard;
    public Integer noble;
}
